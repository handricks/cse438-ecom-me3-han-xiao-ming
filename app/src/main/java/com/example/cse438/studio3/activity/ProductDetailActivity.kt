package com.example.cse438.studio3.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.example.cse438.studio3.R
import com.example.cse438.studio3.model.Product
import com.example.cse438.studio3.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.feature_list_item.view.*

class ProductDetailActivity: AppCompatActivity() {
    private lateinit var product: Product
    private lateinit var viewModel: ProductViewModel

    private lateinit var adapter: FeatureAdapter
    private var features = ArrayList<Feature>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        product = intent.extras!!.getSerializable("PRODUCT") as Product
        Log.e("FAVORITE", product.isFavorite.toString())

        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        this.loadUI(product)
    }

    override fun onBackPressed() {
        this.finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.product_detail_menu, menu)
        if (this.product.isFavorite) {
            menu?.getItem(0)?.icon = getDrawable(R.drawable.ic_star_24dp)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
            R.id.action_favorite -> {
                // TODO: Implement according to the given pseudocode
                if (this.product.isFavorite){
                    item.icon = getDrawable(R.drawable.ic_star_border_24dp)
                    viewModel.removeFavorite(this.product.getId())
                    this.product.isFavorite=false;
                }
                else{
                    item.icon=getDrawable(R.drawable.ic_star_24dp)
                    viewModel.addFavorite(this.product)
                    this.product.isFavorite=true;

                }
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadUI(product: Product) {
        val priceText = "\$${product.getPrice()}"

        product_title.text = product.getProductName()

        if (product.getPrice() == -1.0) {
            decrement.visibility = View.GONE
            quantity.visibility = View.GONE
            increment.visibility = View.GONE

            price.visibility = View.GONE
            btn_add_to_cart.visibility = View.GONE
        }
        else {
            price.text = priceText
            decrement.setOnClickListener {
                var quantityVal = Integer.parseInt(quantity.text.toString())
                if (quantityVal <= 1) {
                    quantityVal = 1
                    Toast.makeText(this@ProductDetailActivity, "Cannot choose quantity less than 1.", Toast.LENGTH_SHORT).show()
                }
                else {
                    quantityVal -= 1
                }
                quantity.text = quantityVal.toString()
            }
            increment.setOnClickListener {
                var quantityVal = Integer.parseInt(quantity.text.toString())
                if (quantityVal >= 10) {
                    quantityVal = 10
                    Toast.makeText(this@ProductDetailActivity, "Cannot choose quantity greater than 10.", Toast.LENGTH_SHORT).show()
                }
                else {
                    quantityVal += 1
                }
                quantity.text = quantityVal.toString()
            }
            btn_add_to_cart.setOnClickListener {
                // do stuff
            }
        }
        product_desc.text = product.getProductDescription()
        manufacturer.text = product.getManufacturer()
        brand.text = product.getBrand()
        model.text = product.getModel()
        weight.text = product.getWeight().toString()
        length.text = product.getLength().toString()
        width.text = product.getWidth().toString()
        height.text = product.getHeight().toString()

        val images = product.getImages()
        if (images.size > 0) {
            Picasso.with(this).load(product.getImages()[0]).into(product_img)
        }
        else {
            // eventually show image not available pic
        }

        val color = product.getProductColor()
        val category = product.getCategory()
        if (color != "") {
            features.add(Feature("Color", color))
        }
        if (category != "") {
            features.add(Feature("Category", category))
        }

        val featureMap = product.getProductFeatures()
        for (mapping in featureMap) {
            if (mapping.key != "") {
                features.add(Feature(mapping.key, mapping.value))
            }
        }

        adapter = FeatureAdapter(this.features)
        features_list.adapter = adapter
        features_list.layoutManager = LinearLayoutManager(this)
    }

    inner class FeatureAdapter(private var items: ArrayList<Feature>): RecyclerView.Adapter<FeatureAdapter.FeatureViewHolder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FeatureViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.feature_list_item, p0, false)
            return FeatureViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(p0: FeatureViewHolder, p1: Int) {
            val item = items[p1]

            val keyText = "${item.key}:"
            p0.key.text = keyText
            p0.value.text = item.value
        }

        inner class FeatureViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val key: TextView = itemView.feature_label
            val value: TextView = itemView.feature
        }
    }

    class Feature(
        var key: String,
        var value: String
    )
}