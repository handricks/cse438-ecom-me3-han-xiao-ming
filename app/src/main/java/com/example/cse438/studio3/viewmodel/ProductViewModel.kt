package com.example.cse438.studio3.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.AsyncTask
import android.util.Log
import com.example.cse438.studio3.db.DbSettings
import com.example.cse438.studio3.db.FavoritesDatabaseHelper
import com.example.cse438.studio3.model.Offer
import com.example.cse438.studio3.model.Product
import com.example.cse438.studio3.model.SiteDetail
import com.example.cse438.studio3.util.QueryUtils

class ProductViewModel(application: Application): AndroidViewModel(application) {
    private var _favouriteDBHelper: FavoritesDatabaseHelper = FavoritesDatabaseHelper(application)
    private var _productsList: MutableLiveData<ArrayList<Product>> = MutableLiveData()

    fun getNewProducts(): MutableLiveData<ArrayList<Product>> {
        loadProducts("?filter={\"where\":{\"is_new\":1}}")
        return _productsList
    }

    fun getProductsByQueryText(query: String): MutableLiveData<ArrayList<Product>> {
        loadProducts("?filter={\"where\":{\"name\":{\"like\":\".*$query.*\",\"options\":\"i\"}}}")
        return _productsList
    }

    private fun loadProducts(query: String) {
        ProductAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Product>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Product>? {
            return QueryUtils.fetchProductData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Product>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                // TODO: Tag Items with favorites
                val fromFavorites = loadFavorites();
                var newFavorites : ArrayList<Product> = ArrayList();
                for (i in result) {
                    for (j in fromFavorites){
                        if (i.getId() == j.getId()) {
                            i.isFavorite = true;
                            break;
                        }
                    }
                    newFavorites.add(i);
                }
                _productsList.value = newFavorites;
            }
        }
    }

    fun getFavorites(): MutableLiveData<ArrayList<Product>> {
        val returnList = this.loadFavorites()
        this._productsList.value = returnList
        return this._productsList
    }

    private fun loadFavorites(): ArrayList<Product> {
        val favorites: ArrayList<Product> = ArrayList()
        val database = this._favouriteDBHelper.readableDatabase

        val cursor = database.query(
            DbSettings.DBFavoriteEntry.TABLE,
            null,
            null, null, null, null, DbSettings.DBFavoriteEntry.COL_FAV_TITLE
        )

        while (cursor.moveToNext()) {
            val cursorId = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.ID)
            val cursorApiId = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_API_ID)
            val cursorTitle = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_FAV_TITLE)
            val cursorDesc = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_DESCRIPTION)
            val cursorManufacturer = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_MANUFACTURER)
            val cursorModel = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_MODEL)
            val cursorBrand = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_BRAND)
            val cursorCatId = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_CATEGORY_ID)
            val cursorCat = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_CATEGORY_NAME)
            val cursorWeight = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_WEIGHT)
            val cursorLength = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_LENGTH)
            val cursorWidth = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_WIDTH)
            val cursorHeight = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_HEIGHT)
            val cursorImgCount = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_IMAGE_COUNT)
            val cursorPrice = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_PRICE)
            val cursorCurrency = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_CURRENCY)
            val cursorColor = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_COLOR)
            val cursorIsNew = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_IS_NEW)
            val cursorUPC = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_UPC)
            val cursorEAN = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_EAN)
            val cursorMPN = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_MPN)
            val cursorCreated = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_CREATED_DATE)
            val cursorUpdated = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_UPDATED_DATE)
            val cursorSem3Id = cursor.getColumnIndex(DbSettings.DBFavoriteEntry.COL_SEM3_ID)

            // TODO: Load up a Cursor object for images similar to how the gtinCursor is loaded
            val imageCursor = database.query(
                DbSettings.DBImageAssetEntry.TABLE,
                arrayOf(
                    DbSettings.DBImageAssetEntry.COL_URL
                ),
                "${DbSettings.DBImageAssetEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val images = ArrayList<String>()
            while (imageCursor.moveToNext()) {
                images.add(imageCursor.getString(imageCursor.getColumnIndex(DbSettings.DBImageAssetEntry.COL_URL)))
            }
            imageCursor.close()
            // TODO: Iterate over the Cursor object above and add the contents to the images ArrayList above; Finally, close the Cursor

            val featureCursor = database.query(
                DbSettings.DBFeatureEntry.TABLE,
                arrayOf(
                    DbSettings.DBFeatureEntry.COL_NAME,
                    DbSettings.DBFeatureEntry.COL_DESCRIPTION
                ),
                "${DbSettings.DBFeatureEntry.FAVORITE_ID}=?", arrayOf(cursor.getString(cursorId).toString()), null, null, null
            )
            val features = HashMap<String, String>()
            while (featureCursor.moveToNext()) {
                features[featureCursor.getString(featureCursor.getColumnIndex(DbSettings.DBFeatureEntry.COL_NAME))] =
                        featureCursor.getString(featureCursor.getColumnIndex(DbSettings.DBFeatureEntry.COL_DESCRIPTION))
            }
            featureCursor.close()

            val gtinCursor = database.query(
                DbSettings.DBGtinEntry.TABLE,
                arrayOf(
                    DbSettings.DBGtinEntry.COL_GTIN
                ),
                "${DbSettings.DBGtinEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val gtins = ArrayList<Long>()
            while (gtinCursor.moveToNext()) {
                gtins.add(gtinCursor.getLong(gtinCursor.getColumnIndex(DbSettings.DBGtinEntry.COL_GTIN)))
            }
            gtinCursor.close()

            val vsCursor = database.query(
                DbSettings.DBVariationSecondaryIdEntry.TABLE,
                arrayOf(
                    DbSettings.DBVariationSecondaryIdEntry.COL_VAR_SEC_ID
                ),
                "${DbSettings.DBVariationSecondaryIdEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val vsIds = ArrayList<String>()
            while (vsCursor.moveToNext()) {
                vsIds.add(vsCursor.getString(vsCursor.getColumnIndex(DbSettings.DBVariationSecondaryIdEntry.COL_VAR_SEC_ID)))
            }
            vsCursor.close()

            val detailCursor = database.query(
                DbSettings.DBSiteDetailEntry.TABLE,
                arrayOf(
                    DbSettings.DBSiteDetailEntry.COL_NAME,
                    DbSettings.DBSiteDetailEntry.COL_SKU,
                    DbSettings.DBSiteDetailEntry.COL_URL
                ),
                "${DbSettings.DBSiteDetailEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val details = ArrayList<SiteDetail>()
            while (detailCursor.moveToNext()) {
                val detailCursorId = detailCursor.getColumnIndex(DbSettings.DBSiteDetailEntry.ID)

                if (detailCursorId != -1) {
                    val offerCursor = database.query(
                        DbSettings.DBOfferEntry.TABLE,
                        arrayOf(
                            DbSettings.DBOfferEntry.COL_API_ID,
                            DbSettings.DBOfferEntry.COL_AVAILABILITY,
                            DbSettings.DBOfferEntry.COL_CONDITION,
                            DbSettings.DBOfferEntry.COL_FIRST_RECORDED,
                            DbSettings.DBOfferEntry.COL_LAST_RECORDED,
                            DbSettings.DBOfferEntry.COL_IS_ACTIVE,
                            DbSettings.DBOfferEntry.COL_SELLER,
                            DbSettings.DBOfferEntry.COL_PRICE,
                            DbSettings.DBOfferEntry.COL_CURRENCY
                        ),
                        "${DbSettings.DBOfferEntry.SITE_DETAIL_ID}=?", arrayOf(detailCursor.getLong(detailCursorId).toString()), null, null, null
                    )
                    val offers = ArrayList<Offer>()
                    while (offerCursor.moveToNext()) {
                        offers.add(
                            Offer(
                                cursor.getString(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_API_ID)),
                                cursor.getString(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_CURRENCY)),
                                cursor.getLong(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_FIRST_RECORDED)),
                                cursor.getDouble(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_PRICE)),
                                cursor.getLong(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_LAST_RECORDED)),
                                cursor.getString(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_SELLER)),
                                cursor.getString(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_CONDITION)),
                                cursor.getString(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_AVAILABILITY)),
                                cursor.getInt(cursor.getColumnIndex(DbSettings.DBOfferEntry.COL_IS_ACTIVE))
                            )
                        )
                    }
                    offerCursor.close()

                    details.add(
                        SiteDetail(
                            cursor.getString(cursor.getColumnIndex(DbSettings.DBSiteDetailEntry.COL_URL)),
                            cursor.getString(cursor.getColumnIndex(DbSettings.DBSiteDetailEntry.COL_NAME)),
                            cursor.getString(cursor.getColumnIndex(DbSettings.DBSiteDetailEntry.COL_SKU)),
                            cursor.getInt(cursor.getColumnIndex(DbSettings.DBSiteDetailEntry.COL_OFFERS_COUNT)),
                            offers
                        )
                    )
                }
            }
            detailCursor.close()

            val geoCursor = database.query(
                DbSettings.DBGeographyEntry.TABLE,
                arrayOf(
                    DbSettings.DBGeographyEntry.COL_GEO
                ),
                "${DbSettings.DBGeographyEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val geos = ArrayList<String>()
            while (geoCursor.moveToNext()) {
                geos.add(geoCursor.getString(geoCursor.getColumnIndex(DbSettings.DBGeographyEntry.COL_GEO)))
            }
            geoCursor.close()

            val msgCursor = database.query(
                DbSettings.DBMessageEntry.TABLE,
                arrayOf(
                    DbSettings.DBMessageEntry.COL_MESSAGE
                ),
                "${DbSettings.DBMessageEntry.FAVORITE_ID}=?", arrayOf(cursor.getLong(cursorId).toString()), null, null, null
            )
            val msgs = ArrayList<String>()
            while (msgCursor.moveToNext()) {
                msgs.add(msgCursor.getString(msgCursor.getColumnIndex(DbSettings.DBMessageEntry.COL_MESSAGE)))
            }
            msgCursor.close()

            val product = Product(
                cursor.getString(cursorApiId),
                cursor.getString(cursorManufacturer),
                cursor.getString(cursorModel),
                cursor.getString(cursorBrand),
                cursor.getInt(cursorCatId),
                cursor.getString(cursorCat),
                cursor.getDouble(cursorWeight),
                cursor.getDouble(cursorLength),
                cursor.getDouble(cursorWidth),
                cursor.getDouble(cursorHeight),
                cursor.getInt(cursorImgCount),
                images,
                cursor.getDouble(cursorPrice),
                cursor.getString(cursorCurrency),
                cursor.getString(cursorTitle),
                cursor.getString(cursorDesc),
                cursor.getString(cursorColor),
                features,
                cursor.getInt(cursorIsNew),
                cursor.getLong(cursorUPC),
                cursor.getLong(cursorEAN),
                cursor.getString(cursorMPN),
                gtins,
                cursor.getLong(cursorCreated),
                cursor.getLong(cursorUpdated),
                vsIds,
                cursor.getString(cursorSem3Id),
                details,
                geos,
                msgs
            )
            product.isFavorite = true
            favorites.add(product)
        }

        cursor.close()
        database.close()

        return favorites
    }

    fun addFavorite(product: Product) {
        val database: SQLiteDatabase = this._favouriteDBHelper.writableDatabase

        val favValues = ContentValues()
        favValues.put(DbSettings.DBFavoriteEntry.COL_API_ID, product.getId())
        favValues.put(DbSettings.DBFavoriteEntry.COL_FAV_TITLE, product.getProductName())
        favValues.put(DbSettings.DBFavoriteEntry.COL_DESCRIPTION, product.getProductDescription())
        favValues.put(DbSettings.DBFavoriteEntry.COL_MANUFACTURER, product.getManufacturer())
        favValues.put(DbSettings.DBFavoriteEntry.COL_MODEL, product.getModel())
        favValues.put(DbSettings.DBFavoriteEntry.COL_BRAND, product.getBrand())
        favValues.put(DbSettings.DBFavoriteEntry.COL_CATEGORY_ID, product.getCategoryId())
        favValues.put(DbSettings.DBFavoriteEntry.COL_CATEGORY_NAME, product.getCategory())
        favValues.put(DbSettings.DBFavoriteEntry.COL_WEIGHT, product.getWeight())
        favValues.put(DbSettings.DBFavoriteEntry.COL_LENGTH, product.getLength())
        favValues.put(DbSettings.DBFavoriteEntry.COL_WIDTH, product.getWidth())
        favValues.put(DbSettings.DBFavoriteEntry.COL_HEIGHT, product.getHeight())
        favValues.put(DbSettings.DBFavoriteEntry.COL_IMAGE_COUNT, product.getImageCount())
        favValues.put(DbSettings.DBFavoriteEntry.COL_PRICE, product.getPrice())
        favValues.put(DbSettings.DBFavoriteEntry.COL_CURRENCY, product.getPriceCurrency())
        favValues.put(DbSettings.DBFavoriteEntry.COL_COLOR, product.getProductColor())
        favValues.put(DbSettings.DBFavoriteEntry.COL_IS_NEW, product.getIsNew())
        favValues.put(DbSettings.DBFavoriteEntry.COL_UPC, product.getUniversalProductNumber())
        favValues.put(DbSettings.DBFavoriteEntry.COL_EAN, product.getEuropeanArticleNumber())
        favValues.put(DbSettings.DBFavoriteEntry.COL_MPN, product.getManufacturerPartNumber())
        favValues.put(DbSettings.DBFavoriteEntry.COL_CREATED_DATE, product.getCreatedDate())
        favValues.put(DbSettings.DBFavoriteEntry.COL_UPDATED_DATE, product.getUpdatedDate())
        favValues.put(DbSettings.DBFavoriteEntry.COL_SEM3_ID, product.getSem3Id())
        val favId = database.insertWithOnConflict(
            DbSettings.DBFavoriteEntry.TABLE,
            null,
            favValues,
            SQLiteDatabase.CONFLICT_REPLACE
        )

        for (image in product.getSem3Images()) {
            val imagevalues = ContentValues()
            imagevalues.put(DbSettings.DBImageAssetEntry.FAVORITE_ID, favId)
            imagevalues.put(DbSettings.DBImageAssetEntry.COL_URL, image)
            database.insertWithOnConflict(
                DbSettings.DBImageAssetEntry.TABLE,
                null,
                imagevalues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }
        // TODO: Loop over the product's sem3 Images and add them to the images table similarly to how the gtins are added below

        for (feature in product.getProductFeatures()) {
            val featureValues = ContentValues()
            featureValues.put(DbSettings.DBFeatureEntry.FAVORITE_ID, favId)
            featureValues.put(DbSettings.DBFeatureEntry.COL_NAME, feature.key)
            featureValues.put(DbSettings.DBFeatureEntry.COL_DESCRIPTION, feature.value)
            database.insertWithOnConflict(
                DbSettings.DBFeatureEntry.TABLE,
                null,
                featureValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }

        for (gtin in product.getGlobalTradeItemNumbers()) {
            val gtinValues = ContentValues()
            gtinValues.put(DbSettings.DBGtinEntry.FAVORITE_ID, favId)
            gtinValues.put(DbSettings.DBGtinEntry.COL_GTIN, gtin)
            database.insertWithOnConflict(
                DbSettings.DBGtinEntry.TABLE,
                null,
                gtinValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }

        for (vsi in product.getVariationSecondaryIds()) {
            val vsiValues = ContentValues()
            vsiValues.put(DbSettings.DBVariationSecondaryIdEntry.FAVORITE_ID, favId)
            vsiValues.put(DbSettings.DBVariationSecondaryIdEntry.COL_VAR_SEC_ID, vsi)
            database.insertWithOnConflict(
                DbSettings.DBVariationSecondaryIdEntry.TABLE,
                null,
                vsiValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }

        for (geo in product.getGeography()) {
            val geoValues = ContentValues()
            geoValues.put(DbSettings.DBGeographyEntry.FAVORITE_ID, favId)
            geoValues.put(DbSettings.DBGeographyEntry.COL_GEO, geo)
            database.insertWithOnConflict(
                DbSettings.DBGeographyEntry.TABLE,
                null,
                geoValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }

        for (message in product.getMessages()) {
            val mesValues = ContentValues()
            mesValues.put(DbSettings.DBMessageEntry.FAVORITE_ID, favId)
            mesValues.put(DbSettings.DBMessageEntry.COL_MESSAGE, message)
            database.insertWithOnConflict(
                DbSettings.DBMessageEntry.TABLE,
                null,
                mesValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }

        for (detail in product.getSiteDetails()) {
            val detailValues = ContentValues()
            detailValues.put(DbSettings.DBSiteDetailEntry.FAVORITE_ID, favId)
            detailValues.put(DbSettings.DBSiteDetailEntry.COL_NAME, detail.getName())
            detailValues.put(DbSettings.DBSiteDetailEntry.COL_SKU, detail.getSKU())
            detailValues.put(DbSettings.DBSiteDetailEntry.COL_URL, detail.getUrl())
            detailValues.put(DbSettings.DBSiteDetailEntry.COL_OFFERS_COUNT, detail.getRecentOffersCount())
            val detailId = database.insertWithOnConflict(
                DbSettings.DBSiteDetailEntry.TABLE,
                null,
                detailValues,
                SQLiteDatabase.CONFLICT_REPLACE
            )

            for (offer in detail.getLatestOffers()) {
                val offerValues = ContentValues()
                offerValues.put(DbSettings.DBOfferEntry.SITE_DETAIL_ID, detailId)
                offerValues.put(DbSettings.DBOfferEntry.COL_API_ID, offer.getId())
                offerValues.put(DbSettings.DBOfferEntry.COL_SELLER, offer.getSeller())
                offerValues.put(DbSettings.DBOfferEntry.COL_AVAILABILITY, offer.getAvailability())
                offerValues.put(DbSettings.DBOfferEntry.COL_CONDITION, offer.getCondition())
                offerValues.put(DbSettings.DBOfferEntry.COL_IS_ACTIVE, offer.getIsActive())
                offerValues.put(DbSettings.DBOfferEntry.COL_FIRST_RECORDED, offer.getFirstRecordedAt())
                offerValues.put(DbSettings.DBOfferEntry.COL_LAST_RECORDED, offer.getLastRecordedAt())
                offerValues.put(DbSettings.DBOfferEntry.COL_PRICE, offer.getPrice())
                offerValues.put(DbSettings.DBOfferEntry.COL_CURRENCY, offer.getCurrency())
                database.insertWithOnConflict(
                    DbSettings.DBOfferEntry.TABLE,
                    null,
                    offerValues,
                    SQLiteDatabase.CONFLICT_REPLACE
                )
            }
        }

        database.close()
    }

    fun removeFavorite(id: String, isFromResultList: Boolean = false) {
        // TODO: Implement this function according to the pseudocode given
        val database: SQLiteDatabase = this._favouriteDBHelper.writableDatabase
        val id_array= arrayOf(id)
        database.delete(DbSettings.DBFavoriteEntry.TABLE,DbSettings.DBFavoriteEntry.COL_API_ID+"=?",id_array)
        database.close()
        var  product_ :Product=Product()
        var copy=_productsList.value
        if(copy!=null){
            for(product in copy){
                product_=product
                if(id==product.getId()) {
                    break
                }
            }
            if(isFromResultList){
                product_.isFavorite=false
            }
            else{
                copy.remove(product_)
            }
            _productsList.value = copy
        }
    }
}